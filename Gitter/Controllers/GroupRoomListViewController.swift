import Foundation
import CoreData

class GroupRoomListViewController: UITableViewController, NSFetchedResultsControllerDelegate {

    var groupId: String?

    @IBOutlet var topicsButton: UIBarButtonItem!

    private var fetchedResultsController: NSFetchedResultsController<Room>?

    override func loadView() {
        super.loadView()

        RestService().getRooms(forGroupId: groupId!) { (error, didSaveNewData) in
            LogIfError("Failed to get rooms for group: \(self.groupId)", error: error)
        }

        let managedObjectContext = CoreDataSingleton.sharedInstance.uiManagedObjectContext
        let roomFetchRequest = NSFetchRequest<Room>(entityName: "Room")
        roomFetchRequest.predicate = NSPredicate(format: "groupId == %@", groupId!)
        roomFetchRequest.sortDescriptors = [NSSortDescriptor(key: "roomMember", ascending: false),
                                            NSSortDescriptor(key: "mentions", ascending: false),
                                            NSSortDescriptor(key: "unreadItems", ascending: false),
                                            NSSortDescriptor(key: "lastAccessTime", ascending:false),
                                            NSSortDescriptor(key: "name", ascending: true)]
        fetchedResultsController = NSFetchedResultsController(fetchRequest: roomFetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: "roomMember", cacheName: nil)
        fetchedResultsController?.delegate = self

        try! fetchedResultsController?.performFetch()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: "AvatarWithBadgeCell", bundle: nil), forCellReuseIdentifier: "AvatarWithBadgeCell")
        tableView.tableFooterView = UIView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationItem.rightBarButtonItem = SecretMenuData().showTopics ? topicsButton : nil
    }

    @IBAction func didSelectTopics(_ sender: UIBarButtonItem) {
        (splitViewController as! RootViewController).navigateTo(topicsForGroupId: groupId!, sender: self)
    }

    @IBAction func didPullToRefresh(_ sender: UIRefreshControl) {
        RestService().getRooms(forGroupId: groupId!) { (error, didSaveNewData) in
            sender.endRefreshing()
            LogIfError("Failed to refresh rooms for group: \(self.groupId)", error: error)
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController?.sections?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return fetchedResultsController?.sections?[section].numberOfObjects ?? 0
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return (fetchedResultsController!.sections![section].objects!.first as! Room).roomMember ? "Your Rooms" : "Other Rooms"
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AvatarWithBadgeCell", for: indexPath) as! AvatarWithBadgeCell
        configureCell(cell, indexPath: indexPath)
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let room = fetchedResultsController!.object(at: indexPath)
        let rootViewController = splitViewController as? RootViewController

        rootViewController!.navigateTo(roomId: room.id, sender: self)
    }

    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        TableViewChangeHandler.applySectionChange(to: tableView, atSectionIndex: sectionIndex, for: type)
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        TableViewChangeHandler.applyRowChange(to: tableView, at: indexPath, for: type, newIndexPath: newIndexPath)
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }

    private func configureCell(_ cell: AvatarWithBadgeCell, indexPath: IndexPath) {
        let room = fetchedResultsController!.object(at: indexPath)

        AvatarUtils.sharedInstance.configure(cell.avatar, room: room, size: AvatarWithBadgeCell.avatarHeight)
        cell.mainLabel.text = room.name
        cell.sideLabel.text = nil
        cell.bottomLabel.text = nil
        if (room.intMentions > 0) {
            cell.badgeType = .mention
            cell.badgeLabel.text = "@"
        } else if (room.intUnreadItems > 0) {
            cell.badgeType = .unread
            cell.badgeLabel.text = room.intUnreadItems > 99 ? "99+" : String(room.intUnreadItems)
        } else if (room.activity && room.lurk) {
            cell.badgeType = .activityIndicator
            cell.badgeLabel.text = nil
        } else {
            cell.badgeType = .none
            cell.badgeLabel.text = nil
        }
    }
}
