import Foundation

class AvatarCell: UITableViewCell {

    @IBOutlet var avatar: UIImageView!
    @IBOutlet var title: UILabel!

    override func layoutSubviews() {
        super.layoutSubviews()

        // separator lines at the title, not the avatar
        separatorInset = UIEdgeInsets.init(top: 0, left: title.frame.origin.x , bottom: 0, right: 0)

        // rounded corners
        avatar.layer.cornerRadius = avatar.frame.size.width / 8
        avatar.clipsToBounds = true
    }
}
